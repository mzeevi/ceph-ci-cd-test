import boto3
import botocore


def establish_connection(ceph_endpoint, access_key, secret_key):
    try:
        s3_client = boto3.client('s3',
                            endpoint_url = ceph_endpoint,
                            aws_access_key_id = access_key,
                            aws_secret_access_key = secret_key,
                            config = botocore.client.Config(signature_version='s3'))
    except:
        print("Connection failed")

    return s3_client


def create_s3_resource(ceph_endpoint, access_key, secret_key):
    try:
        s3_resource = boto3.resource('s3',
                            endpoint_url = ceph_endpoint,
                            aws_access_key_id = access_key,
                            aws_secret_access_key = secret_key,
                            config = botocore.client.Config(signature_version='s3'))
    except:
        print("Resource failed")

    return s3_resource


def create_bucket(s3_client, bucket_name):
    try:
        s3_client.create_bucket(Bucket = bucket_name)
    except:
        print("Bucket already exists")


def enable_bucket_versioning(s3_resource, bucket_name):
    try:
        bucket_versioning = s3_resource.BucketVersioning(bucket_name)
        bucket_versioning.enable()
    except:
        print("Versioning failed")


def enable_lifecycle_rule(s3_client, bucket_name):
    try:
        s3_client.put_bucket_lifecycle_configuration(
            Bucket=bucket_name,
            LifecycleConfiguration=
            {
                "Rules": [
                    {
                        "ID": "NonCurrent1Day",
                        "Prefix": "",
                        "Status": "Enabled",
                        "NoncurrentVersionExpiration": {
                            "NoncurrentDays": 1
                        }
                    }
                ]
            }
        )
    except:
        print("LC failed")

def upload_object(s3_client, bucket_name, object_path, object_name):
    try:
        s3_client.upload_file(object_path, bucket_name, object_name)
    except:
        print("Upload failed")


if __name__ == "__main__":
    ceph_endpoint = "http://mon0:8080"
    access_key = "H7JSRTXB8CFY11S4P1GL"
    secret_key = "qCGECA0SHcV54QteHf45Q6sAZyriA72mMIPYFU9N"
    bucket_name = "ci-test"
    object_path = "./output/complete_output.txt"
    object_name = "complete_output.txt"

    s3_client = establish_connection(ceph_endpoint, access_key, secret_key)
    s3_resource = create_s3_resource(ceph_endpoint, access_key, secret_key)

    create_bucket(s3_client, bucket_name)
    enable_bucket_versioning(s3_resource, bucket_name)
    enable_lifecycle_rule(s3_client, bucket_name)
    upload_object(s3_client, bucket_name, object_path, object_name)
